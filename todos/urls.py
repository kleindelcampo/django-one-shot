#Register that view in the todos app for the
# path "" and the name
#"todo_list_list" in a new file named todos/urls.py.

from django.urls import path
from todos.views import todolist_list, todo_list_detail, create_list, edit_list, delete_list, create_item, update_item

urlpatterns = [
    path('', todolist_list, name="todolist_list"),
    path('<int:id>', todo_list_detail, name="todo_list_detail"),
    path('create/', create_list, name="create_list"),
    path('<int:id>/edit', edit_list, name="edit_list"),
    path('<int:id>/delete', delete_list, name="delete_list"),
    path('items/create', create_item, name="create_item"),
    path('items/<int:id>/edit', update_item, name="update_item"),
]

#for navigation = url name
#for render = html
