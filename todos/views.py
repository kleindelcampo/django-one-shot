from django.shortcuts import get_object_or_404, render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import ListForm, CreateForm
# Create your views here.

def todolist_list(request):
    list = TodoList.objects.all()

    context = {
        "to_do_list": list,
    }
    return render(request, "todolist/list.html", context)

def todo_list_detail(request, id):
    get_list = get_object_or_404(TodoList, id=id)
    #print(get_list.items.all()) to check
    context = {
        "list_object": get_list,

    }
    return render(request, "todolist/detail.html", context)

def create_list(request):
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid():
            #save new form in new_list
            new_list = form.save()
            return redirect("todo_list_detail", new_list.id)

    else:
        form = ListForm()
    context = {
        "form":  form,
    }
    return render(request, "todolist/create_list.html", context)


def edit_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = ListForm(request.POST, instance=list)
        if form.is_valid():
            new_form = form.save()
            return redirect("todo_list_detail", new_form.id)
    else:
        form = ListForm(instance=list)

    context = {
        "list_object": list,
        "form": form,
    }
    return render(request, "todolist/edit_list.html", context)

def delete_list(request, id):
    delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete.delete()
        return redirect('todolist_list')
    return render(request, 'todolist/delete_list.html')

def create_item(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            new_item = form.save()
        return redirect("todo_list_detail", new_item.list.id)
    else:
        form = CreateForm()

    context = {
        "form": form,
    }
    return render(request, "todolist/create_item.html", context)

def update_item(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = CreateForm(request.POST, instance=item)
        if form.is_valid():
            new_form = form.save()
            return redirect("todo_list_detail", new_form.list.id)
    else:
        form = CreateForm(instance=item)

    context = {
        "form": form,
    }
    return render(request, "todolist/update_item.html", context)
