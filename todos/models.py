from django.db import models
from django.conf import settings

# Create your models here.


class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.name


class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(
        null=True,
        blank=True,
    )
    is_completed = models.BooleanField(default=False)
    list = models.ForeignKey(
        #todoList, references the model above.
        TodoList,
        related_name="items",
        on_delete=models.CASCADE,
        null=True,
    )

#if you have the list and need the items use the related name
#if you have the items and need the list use "list"
def edit_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = ListForm(request.POST, instance=list)
        if form.is_valid():
            new_form = form.save()
            return redirect("todo_list_detail", new_form.id)
    else:
        form = ListForm(instance=list)

    context = {
        "list_object": list,
        "form": form,
    }
    return render(request, "todolist/edit_list.html", context)
